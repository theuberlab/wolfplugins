# HTTPBasic Authorization Plugin

The HTTPBasic Authorization Plugin minimally implements HTTPBasic auth
allowing you to provide a separate file containing a list of username:password pairs.

This is provided as a simple example plugin rather than an attempt to
provide any real security and as a result there is currently no support 
for (nor intention of supporting) any form of external credentials database
or encryption of credentials within TheWolf's config file.

This means your passwords will be in clear text within a configuration 
file potentially residing on a disk somewhere and (hopefully) residing in
a git repository within your organization.

This is commonly agreed to be a REALLY BAD IDEA.

TheUberlab highly recommends that you DO NOT use this plugin in production.


