package main

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/go-validator/validator"

	"bitbucket.org/theuberlab/lug"
	"bitbucket.org/theuberlab/thewolf/plugins"
)

// Need to come up with some consistency on how we handle plugins managing their own config. This is a start.
type hTTPBasicConfig struct {
	Realm 			string `yaml:"Realm,omitempty"`// The security realm to send in WWW-Authenticate header in the 401 challenge. Probably not actually neccessary.
	CredsFile		string `yaml:"CredentialsFile" validate:"nonzero"`// The path to the file containing credential pairs. Currently the creds file should contain a list of entries in the form "username:password" There is no intent to support anything more advanced than this (like per-action credentials.) If that is desired a new plugin which could use external sources (such as LDAP or Active Directory) would be preferred over continuing development work on this plugin.
}

//TODO: Update the plugin interfaces for the return types on ValidatePluginConfig and the other validate config method.

//TODO: Come up with a way to have a constructor for new plugins. Right now the way I'm fetching them I actually get an instance due to the golang plugin mechanism. Maybe instead of fetching the interface I just fetch a New<Type>Plugin(configuration Plugin<Type>Config) method? How will that work with plugins defining their own config? just an interface?
//TODO: Figure out if there's a way to Add the below method to them as well
//func NewAuthPlugin() *plugins.AuthorizationPlugin {
//	httBas := new(plugins.AuthorizationPlugin)
//	httBas.
//
//	return httBas
//}

// Not actually a string, just a handle for all our methods.
type plugin struct {
	config 			*hTTPBasicConfig
	realm			*string
	credendials 	map[string]string 	// This is a SUPER insecure way to store credentials in memory.
}

// Expose ourselves as a public variable so that all our methods can be fetched.
var WolfPlugin plugin

//TODO: Move these into the type when we create the factory method. But only if we can esure they are set.
var (
	name		= "HTTPBasic"
	version 	= "0.0.1"
	pluginType 	= plugins.PLUGIN_TYPE_AUTHORIZATION
)

// Example from the validator readme. https://github.com/go-validator/validator
//type NewUserRequest struct {
//	Username string `validate:"min=3,max=40,regexp=^[a-zA-Z]*$"`
//	Name string     `validate:"nonzero"`
//	Age int         `validate:"min=21"`
//	Password string `validate:"min=8"`
//}
//
//nur := NewUserRequest{Username: "something", Age: 20}
//if errs := validator.Validate(nur); errs != nil {
//// values not valid, deal with errors here
//}

////Authorization: Basic username:password
//func (p *plugin) GetAuthorizationFunction() func(http.HandlerFunc) http.HandlerFunc {
//	return basicAuth
//}
//
//func (p *plugin) GetAuthorizationFunctionWithConfig(config plugins.AuthorizationPluginConfig)  func(http.HandlerFunc) http.HandlerFunc {
//	// Something like this config.Configuration["Credentialfile"]
//
//	return basicAuth
//}

//Authorization: Basic username:password
func (p *plugin) GetAuthorizationFunction() func(http.HandlerFunc) http.HandlerFunc {
	return p.performBasic
}

func (p *plugin) LogSomething() {
	lug.Debug("Message", "Something")
}

//TODO: Replace this functionality with proper plugin "initialization" and a factory function.

// This is kind of a stupid hack which means we're reading the auth file every time we are called.
func (p *plugin) GetAuthorizationFunctionWithConfig(config plugins.AuthorizationPluginConfig) func(http.HandlerFunc) http.HandlerFunc {
	lug.Debug("Messge", "Returning Authoirzation Function", "Config", fmt.Sprintf("%v", config))
	//executorConfig := instance.Config.Config.(map[string]interface{})

	p.LogSomething()

	myConfig, ok := config.Configuration.(map[string]interface{})
	if !ok {
		lug.Error("Message", "Unable to read plugin config")
	}

	p.config = &hTTPBasicConfig {
		Realm:		myConfig["Realm"].(string),
		CredsFile:	myConfig["CredentialsFile"].(string),
	}
	p.realm = &p.config.Realm
	//p.config.Realm = myConfig["Realm"].(string)
	//p.config.CredsFile = myConfig["CredentialsFile"].(string)
	p.credendials = parseCredsFile(p.config.CredsFile)

	return p.performBasic
}

//TODO: Replace this with a factory function. We can't even currently call this the way we're using the type
func (p *plugin) InitPlugin(config plugins.AuthorizationPluginConfig) {
	myConfig, ok := config.Configuration.(hTTPBasicConfig)
	if !ok {
		lug.Error("Message", "Unable to read plugin config")
	}

	p.config.Realm = myConfig.Realm
	p.config.CredsFile = myConfig.CredsFile
	p.credendials = parseCredsFile(p.config.CredsFile)
}

// Parses the supplied file for username:password pairs and turns them into a map.
func parseCredsFile(credsfile string) map[string]string {

	credPairs := make(map[string]string)

	file, err := os.Open(credsfile)

	if err != nil {
		lug.Error("Message", "Failed to open credentials file", "CredentialsFile", credsfile, "Error", err)
		os.Exit(1)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	//TODO: Ignore comment lines
	for scanner.Scan() {
		pair := strings.Split(scanner.Text(), ":")
		credPairs[pair[0]] = pair[1]
	}

	if err := scanner.Err(); err != nil {
		lug.Error("Message", "Unable to read credentials file", "CredentialsFile", credsfile, "Error", err)
	}

	return credPairs
}

// returns a function which will check for basic auth and then call ServeHTTP(w, r) on the supplied function.
//Authorization: Basic user1:password1
// encodes as:
//Authorization: Basic dXNlcjE6cGFzc3dvcmQx
func (p *plugin) performBasic(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//TODO: I think I only want to set this if we're returning a 401.
		w.Header().Set("WWW-Authenticate", "Basic realm=\"" + *p.realm + "\"")


		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			lug.Debug("Message", "Authorization header not found")
			http.Error(w, "Not authorized", 401)
			return
		}

		authComponents := strings.Split(authHeader, " ")
		if len(authComponents) != 2 {
			lug.Debug("Message", "Did not understand authorization header", "HeaderValue", authComponents)
			http.Error(w, "Not authorized", 401)
			return
		}

		//Normalize the first 1/2 of the authorization header and make sure it's basic
		authComponents[0] = strings.ToLower(authComponents[0])
		if authComponents[0] != "basic" {
			lug.Debug("Message", "Authorization mechanism not supported", "Mechanism", authComponents[0])
			http.Error(w, "Not authorized", 401)
			return
		}

		decodedAuth, err := base64.StdEncoding.DecodeString(authComponents[1])
		if err != nil {
			lug.Error("Message", "Error decoding authoiraztion header", "Error", err)
			http.Error(w, err.Error(), 401)
			return
		}
		lug.Trace("Message", "Decoded authorization header", "Value", decodedAuth)
		authPair := strings.SplitN(string(decodedAuth), ":", 2)
		if len(authPair) != 2 {
			lug.Error("Message", "Incorrect number of values in authoirzation header.")
			http.Error(w, "Incorrect authorization format", 401)
			return
		}
		lug.Debug("Message", "Decoded authorization header", "Username", authPair[0])


		if p.credendials[authPair[0]] != authPair[1] {
			lug.Debug("Message", "Invalid credentials supplied")
			http.Error(w, "Not Authorized", 401)
			return
		}

		handlerFunc.ServeHTTP(w, r)
	}
}

// Return the plugin's name
func (p *plugin) GetPluginName() string {
	return name
}

// Returns a version in symantic format.
func (p *plugin) GetPluginVersion() string {
	return version
}

func (p *plugin) GetPluginType() plugins.PluginType {
	return pluginType
}

// Actually validate the config file section
func (p *plugin) ValidatePluginConfig(config interface{}) error {
	return validator.Validate(config.(hTTPBasicConfig))
}