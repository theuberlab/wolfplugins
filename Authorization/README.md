# Authoriaztion Plugins

Before taking action on a request TheWolf needs to determine if the system 
or person making a request to TheWolf is allowed to perform the specified action.

This is implemented by the use of Authorization plugins.

Authorization plugins will act on the actual request recieved by the API and either 
reject it or pass it on optionally modifying it before passing it.

This allows Authorization plugins to be chained together and allows subsequent
plugins to authorize based on data inserted into the request by a prior plugin.

The hope is that this will encourange the development of authorization plugins by 
spreading the workload. For example, header validation alone is a poor form 
of authorization. However a plugin which fetches and decodes a token from
a remote system could set a header which the subequent header plugin validates.   

## Authorization flow
So far we have this:


1)  Client makes HTTPS request to api
1)  Based on the configuration TheWolf passes (w http.ResponseWriter, r *http.Request) plus it's config and a handle to the PluginManager to the first authentication plugin.
1)  Said plugin does what it is configured to do. If authorization fails it returns an HTTP Authorization Failed error.
1)  If not said plugin MAY modify the http.Request headers
1)  If configured to do so said plugin fetches a handle to the next plugin and passes the full request to said next plugin. Repeating the previous steps.
1)  Once the final plugin returns a positive result that result bubbles up through each plugin back to TheWolf
1)  If the authorization is successful TheWolf proceeds with determining the action which has been called and triggering a job.

NOTES:
*  There is no "login" event and thus no "login session timeout" or "logout" event. Every request to the API is expected to trigger an action (with the exception of redirect based authn/z flows such as OIDC and SAML) and thus every request must be authorized.

## Some questions still un-answered

*  Do we want the plugins to return or the last plugin passes the request to the Listener or parser?
*  How does this work once we add Listeners. Right now we assume everything is HTTP. But eventua
lly we will need integrations with other protocols. Especially SMTP. 
Ideally the Listener defines the protocol. In that case we will have to 
let the listener open the network socket. The problem with that is that 
security is PARAMOUNT in TheWolf. We CANNOT have a plugin do something
insecure with a request. We need to have requests go through an authorization 
plugin before being passed even to a parser and definately before being
passed to any action plugin. So is there a way to enforce this without
being the only ones who create network sockets?
*  How does the current blocking model work with protocols which require 
multiple http requests. I'm pretty sure that breaks some of them. This might not be much 
of an issue though. OAuth for example uses a Bearer Token. If the request contains a 
correct one it is authorized. If it does not the failure actually should usually just be 
the redirect to the external system which causes the authentication/authorization flow.
*  How do we add the ability to authorize individual requests? In order to determine
which action is being called we will need to parse the request. Which conflicts
with the idea of authorizing before parsing. Perhaps we need a pre and post parsing 
authorization phase. Then the authorizer config can specify when it happens?
*  Should Authorization plugins be able to make use of auth source plugins? 
I.E. If someone wants to point perform HTTPBasic auth but specify an Active Directory
backend they would currently have to write a separate plugin which also implements HTTP Basic.
Or if they wanted to simply point the HTTPBasic plugin at a GPG encrypted file or an S3 bucket. 
