PROJECT_ROOT := $(shell git rev-parse --show-toplevel)
include $(PROJECT_ROOT)/Makefile.variables
# Vars
# This is the home of dynamically defined variables, variable paths etc.
# All other vars should live in Makefile.variables.
BIN_DIR := $(GOPATH)/bin
GOMETALINTER := $(BIN_DIR)/gometalinter
#TODO: Start using a plugins version, keep it in sync with the main version?
VERSION ?= $(MAIN_VERSION)
BUILD ?= `date -u +build-%Y%m%d.%H%M%S` # Not actually using this yet but I want to.
os = $(word 1, $@)
currOS := $(shell uname | tr '[:upper:]' '[:lower:]')
BUILDDIR := $(PROJECT_ROOT)/build
# Temporarily using this until we are publishing automatially. #TODO: This should _probably_ be under BUILDDIR if we keep it.
OUTPUTDIR := $(PROJECT_ROOT)/release

## This works to only re-compile the binary if the file main.go when the target is a constant.
## I want to continue to do that so I will have to do a little dance. Perhaps version doesn't include the second, only the day or a hash or something.
## Then I will have to make the target be the file name instead of just the platform.
#$(currOS): pkg/main.go
#	@echo "Made for $(currOS)"
##	GOOS=darwin GOARCH=amd64 go build -ldflags "-X main.Version=$(VERSION)"

#pkg/main.go:


##TODO: Update these two targets to work in a pipeline. See ticket
#.PHONY: deploy
#deploy:
#	@echo "Deploying"
#	if [[ "$(namespace)" == "" ]]; then          \
#	    read -ep "Target namespace: " namespace; \
#	fi;                                          \
#	if [[ "$${namespace}" != "" ]]; then         \
#	    kubectl -n $${namespace} apply           \
#	        --filename k8s-resources             \
#	        --record;                            \
#    fi
#
#.PHONY: teardown
#teardown:
#	@echo "Tearing Down"
#	if [[ "$(namespace)" == "" ]]; then                   \
#	    read -ep "Target namespace: " namespace;          \
#	fi;                                                   \
#	if [[ "$${namespace}" != "" ]]; then                  \
#	    kubectl -n $${namespace} delete -f k8s-resources; \
#	fi

#TODO: Make this work with the new binary file name and directory.
serve: $(currOS)
	$(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

#TODO: Make this bit work
$(OUTPUTDIR):
	mkdir $(OUTPUTDIR)

.PHONY: clean
clean:
	rm $(BUILDDIR)/*
	rm $(OUTPUTDIR)/*

.PHONY: test
test: lint
	go test $(PKGS)

#TODO: This is hacky as crap, make it better.
.PHONY: e2e
e2e: $(currOS)
	echo "Starting The Wolf"
	$(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64 -c $(PROJECT_ROOT)/tests/e2eTests/thewolfconfig.yml &
	go test -v ./tests/e2e_test.go
	pkill thewolf

$(GOMETALINTER):
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install &> /dev/null

.PHONY: lint
lint: $(GOMETALINTER)
	gometalinter ./... --vendor


withbazel:
	bazel build //Action/ExternalCommandExecutor:ExternalCommandExecutor.so
	bazel build //Authorization/HTTPBasic:HTTPBasic.so
	bazel build //Action/TEXTResponder:TEXTResponder.so

#.PHONY: $(PLATFORMS)
#TODO: Make this work. Right now we're not actually doing the .pkg thing but we should so that we get to use the root of the repo for additional things without polluting it so much.
$(PLATFORMS): $(BUILDDIR)
	@echo "Making for $(os)"
	@for pluginSource in $(PKGS); do \
		pluginOutLong=$$(basename $$pluginSource); \
		pluginOut=$${pluginOutLong%%.*}; \
		echo "Making plugin $$pluginOut"; \
		echo "Issuing command GOOS=$(os) GOARCH=amd64 go build -buildmode=plugin -o $(OUTPUTDIR)/$(os)/$${pluginOut}-$(VERSION)-$(os)-amd64.so $$pluginSource"; \
		CGO_ENABLED=0 GOOS=$(os) GOARCH=amd64 go build -buildmode=plugin -o $(OUTPUTDIR)/$(os)/$${pluginOut}-$(VERSION)-$(os)-amd64.so $$pluginSource; \
	done
#	GOOS=$(os) GOARCH=amd64 go build  -o $(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(os)-amd64 .
#	GOOS=$(os) GOARCH=amd64 go build -ldflags "-X main.Version=$(VERSION)" -o $(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(os)-amd64 ./pkg

# Actually build the binaries
.PHONY: release
release: linux darwin
