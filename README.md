# TheWolf Plugins

This repo includes the common plugins which are bundled with 
[TheWolf](https://bitbucket.org/theuberlab/thewolf) as well as documentation
for writing your own plugins.


# About Plugins
Rather than spend a lot of effort writing a large body of code which
almost meets the needs of a lot of organizations and never actually
meets the needs of anyone a primary design factor of [TheWolf](https://bitbucket.org/theuberlab/thewolf)
is that the core body of code actually do as little as possible.

Rather we should focus on providing a mechanism which enables and manages the
interaction of external plugins and a solid foundation of core plugins
which implement base level functionality.


## Plugin Types

The current proposal consists of the following types of plugins. This list
is not solidified so not only may the names change but some may be 
abandoned, added or combined.

Currently only Action plugins are actually supported.

# Action
Action plugins are the ones that actually DO things. Execute command line commands, ssh to servers, issue AWS API calls, etc.

# Listener
Listener plugins provide mechanisms for triggering Actions or series
of actions from an external source. This could include listening for an 
API request in various formats, triggering based on a timer, accepting an email.

# Parser
Parsers are the plugins which will take the body of a request in
whichever format and convert it to an internal Job object. They should
also validate the request though an argument could be made for that to be separate.

# Authorization
Authorization plugins will control the acceptance of TheWolf's job triggers.

# Authentication Source
Authentication Source plugins will enable TheWolf Actions to fetch the 
credentials needed to interact with external systems. Examples could include
SSH keys, kubernetes secrets, certificate stores, fetching a username 
and password from stores such as Vault




# Plugin development

All plugins must implement the interface WolfPlugin which includes 
limited base functionality as well as the interface for the specific 
plugin type.


Currently only ActionPlugin is implemented.

In order to remain flexible plugins must be allowed to specify their own
configuration. To prevent run time errors plugins will have to provide
validator methods. All will likely have to provide a validator for global
plugin settings and Action plugins will need to provide a separate validator
for Action configuration.
